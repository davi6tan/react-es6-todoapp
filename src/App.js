import React, {Component} from 'react';
import {Modal} from 'react-bootstrap';
import './App.css';

class EditForm extends React.Component {
    constructor(props) {
        super(props);
        this.todo = props.todo
        this._handleEdit = this._handleEdit.bind(this);
        this._onSubmit = this._onSubmit.bind(this);

    }

    _handleEdit(e) {
        e.preventDefault();
        this.props.onSetEditTodo(this.todo.id);

    }

    _onSubmit(e) {
        e.preventDefault();
        const text = this._todoText.value.trim();
        this.props.onSaveEditTodo(this.todo.id, text);
    }

    render() {
        return (
            <form onSubmit={this._onSubmit}>
                <div id="todo-list">
                    <p>
                        {this.todo.edit
                            ? <input type="text"
                                     defaultValue={this.todo.text}
                                     ref={input => this._todoText = input}
                            />
                            : <label onDoubleClick={this._handleEdit}>{this.todo.text}</label>
                        }

                        <input
                            type="button"
                            className="todo-row-button btn img-circle"
                            onClick={e => this.props.onSetRemoveTodo(this.todo, e)}>
                        </input>
                    </p>
                </div>
            </form>
        );
    }
}

class TodoForm extends React.Component {

    constructor(props) {
        super(props)
        this._onSubmit = this._onSubmit.bind(this);
    }

    render() {
        return (
            <form onSubmit={this._onSubmit}>
                <div className="input-box">
                    <p><input type="text"
                              ref={input => this._todoText = input}
                              placeholder="What needs to be done? "
                    /></p>


                </div>
            </form>


        )
    }

    focusInput() {
        this._todoText.focus();
    }

    _onSubmit(e) {
        e.preventDefault();
        /// grab text
        const text = this._todoText.value.trim();
        if (text.length === 0)return; //end

        this._todoText.value = "";

        this.props.onAddTodo(text);
    }


}

TodoForm.propTypes = {
    onAddTodo: React.PropTypes.func.isRequired
}

EditForm.propTypes = {
    onSaveEditTodo: React.PropTypes.func.isRequired,
    onSetEditTodo: React.PropTypes.func.isRequired
}

class App extends Component {
    constructor(props) {
        super(props);
        this._nextTodoId = 1;
        this.state = {todos: []}

        this._onAddTodo = this._onAddTodo.bind(this);
        this._onSetRemoveTodo = this._onSetRemoveTodo.bind(this);
        this._onSaveEditTodo = this._onSaveEditTodo.bind(this);
        this._onSetEditTodo = this._onSetEditTodo.bind(this);
    }

    componentDidMount() {
        this._todoForm.focusInput();
    }

    render() {
        const {todos} = this.state;

        return (
            <div>
                <div className="container">
                    <div className="static-modal">
                        <Modal.Dialog>
                            <span className="todo-title">todos</span>

                            <div id="scroll-wrap">
                                <TodoForm
                                    onAddTodo={this._onAddTodo}
                                    ref={form => this._todoForm = form}
                                />
                                <div>{
                                    todos.map(t =>
                                        <div key={t.id}><EditForm
                                            todo={t}
                                            onSetRemoveTodo={this._onSetRemoveTodo}
                                            onSaveEditTodo={this._onSaveEditTodo}
                                            onSetEditTodo={this._onSetEditTodo}
                                        /></div>
                                    )}
                                </div>
                            </div>

                            <Modal.Footer></Modal.Footer>

                        </Modal.Dialog>
                    </div>

                </div>
            </div>


        );
    }

    _onSetRemoveTodo(todo) {
        const current_id = todo.id;
        let {todos} = this.state;
        this.setState({todos: todos.filter(t => t.id !== current_id)})
    }

    _onAddTodo(text) {
        this.setState({
            todos: this.state.todos.concat({
                id: this._nextTodoId++,
                text,
                edit: false
            })
        })

    }

    _onSaveEditTodo(id, text) {
        this.setState({ //save this state
            todos: this.state.todos.map(t => {
                let tmp = t
                if (t.id === id) {
                    tmp.text = text
                    tmp.edit = false
                }
                return tmp;
            })
        })
    }

    _onSetEditTodo(id) {//true to edit
        this.setState({ //save this state
            todos: this.state.todos.map(t => {
                let tmp = t
                if (t.id === id) {
                    tmp.edit = true
                }
                return tmp;
            })
        })
    }


}

export default App;
