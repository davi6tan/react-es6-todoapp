# Simple React Todo App in ES6

*To run* 

    npm install
    npm start

- [Todo App](https://todo-react-es6.herokuapp.com/)



### Tools used

- [React Docs](https://facebook.github.io/react/docs/hello-world.html)
- [React Bootstrap](https://react-bootstrap.github.io/components.html)